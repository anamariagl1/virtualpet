using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;
    [SerializeField] private SO_Pet stats_puppy;
    [SerializeField] private SO_Pet stats_kitty;
    [SerializeField]private SO_Partida m_partida;
    public SO_Partida partida
    {
        get { return m_partida; }
    }
    [SerializeField] private SO_Food m_food;

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

    }
    // Start is called before the first frame update
    void Start()
    {
        m_partida.tActual= DateTime.Now.ToString();
        print("Mascota elegida: "+m_partida.id_mascota);
        print("Hora actual: "+m_partida.tActual.ToString());
        print("Hora guardada: "+m_partida.tAnterior.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ElegirMascota(int num)
    {
        print(num);
        m_partida.id_mascota = num;
    }

    public void Reiniciar()
    {
        print("Reinicio");
        stats_puppy.happiness= stats_puppy.maxHappiness;
        stats_puppy.food = stats_puppy.maxFood;
        stats_puppy.fun = stats_puppy.maxFun;
        stats_puppy.sleep = stats_puppy.maxSleep;

        stats_kitty.happiness = stats_kitty.maxHappiness;
        stats_kitty.food = stats_kitty.maxFood;
        stats_kitty.fun = stats_kitty.maxFun;
        stats_kitty.sleep = stats_kitty.maxSleep;

        m_partida.amount = 100;
        m_food.capacity = 0;
    }

    

    private void OnApplicationQuit()
    {
        m_partida.tAnterior = m_partida.tActual;
        print("Saliendo de la aplicación " + m_partida.tAnterior);
    }

}
