using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Food : ScriptableObject
{
    public int maxCapacity;
    public int capacity;
    public int price;
}
