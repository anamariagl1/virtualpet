using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Partida : ScriptableObject
{
    public int amount;
    public int id_mascota;
    public string tAnterior;
    public string tActual;
}
