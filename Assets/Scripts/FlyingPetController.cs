
using UnityEngine;
using UnityEngine.InputSystem;

public class FlyingPetController : MonoBehaviour
{

    public delegate void Morirse();
    public event Morirse OnMorirse;
    [SerializeField] private InputActionAsset InputAsset;
    private InputActionAsset Input;
    [SerializeField] private PointsManager pointsManager;
    [SerializeField] private GameObject gameManager;
    [SerializeField] private ButtonController button;
    [SerializeField] private Sprite puppy;
    [SerializeField] private Sprite kitty;
    [SerializeField] private float jumpForce;
    private void Awake()
    {
        gameManager = GameObject.Find("GameManager");
        Input = Instantiate(InputAsset);
        Input.FindActionMap("FlyingMinigame").FindAction("Click").performed += Volar;
        Input.FindActionMap("FlyingMinigame").Enable();

        OnMorirse += pointsManager.sumarPuntosATotal;

        switch (gameManager.GetComponent<GameManager>().partida.id_mascota)
        {
            case 0:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = puppy;
                break;
            case 1:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = kitty;
                break;

        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.position.y > 6.5f || this.transform.position.y < - 6.5f)
        {
            EndGame();
        }
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if(other.tag == "Bad"){

           EndGame();
        }
    }

    private void Volar(InputAction.CallbackContext input)
    {
        print("Vuelo");
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
    }

    private void EndGame()
    {
        OnMorirse?.Invoke();
        Input.FindActionMap("FlyingMinigame").Disable();
        button.EndMinigame();
    }
}
