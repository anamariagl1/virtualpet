using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class FoodController : MonoBehaviour
{
    public delegate void Comer();
    public event Comer OnComer;
    public SO_Food food;
    public SO_Partida money;
    [SerializeField] private GameObject pet;
    [SerializeField] private MoneyManager moneyManager;
    [SerializeField] private Sprite[] food_sprites;


    // Start is called before the first frame update
    void Start()
    {
        OnComer += pet.GetComponent<PetController>().Comer; 
        
        if (food.capacity > 70)
        {
            this.GetComponent<SpriteRenderer>().sprite = food_sprites[0];
        }
        else if (food.capacity > 30)
        {

            this.GetComponent<SpriteRenderer>().sprite = food_sprites[1];
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = food_sprites[2];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (food.capacity < food.maxCapacity && money.amount >= food.price)
        {
            GetFood();
            moneyManager.restarCantidad(food.price);
        }
        else if(food.capacity > 0)
        {
            OnComer?.Invoke();
        }
        if (food.capacity > 80)
        {
            this.GetComponent<SpriteRenderer>().sprite = food_sprites[0];
        }
    }

    public void GetFood()
    {
        food.capacity = food.maxCapacity;
        print("A comer");
        OnComer?.Invoke();
                
    }

    public void LoseFood(int amount)
    {
        if(food.capacity > 0 )
        {
            food.capacity -= amount;
        }
        if (food.capacity > 70)
        {
            this.GetComponent<SpriteRenderer>().sprite = food_sprites[0];
        }
        else if (food.capacity > 30)
        {

            this.GetComponent<SpriteRenderer>().sprite = food_sprites[1];
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = food_sprites[2];
        }
    }
}
