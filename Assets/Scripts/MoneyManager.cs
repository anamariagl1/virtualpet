using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    [SerializeField] private SO_Partida money;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = money.amount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void sumarCantidad(int num)
    {
        money.amount += num;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = money.amount.ToString();
    }

    public void restarCantidad(int num)
    {
        if (money.amount - num >= 0)
        {
            money.amount -= num;
            this.GetComponent<TMPro.TextMeshProUGUI>().text = money.amount.ToString();

        }
    }

    
}
