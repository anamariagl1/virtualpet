using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    [SerializeField] private GameObject pet;
    private SO_Pet stats;

    void Start()
    {
        this.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Actualizar()
    {
        stats = pet.GetComponent<PetController>().Stats;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Food: " + stats.food + "\nSleep: " + stats.sleep+ "\nFun: " + stats.fun + "\nHappiness: " + stats.happiness;

        if(stats.food>=stats.maxFood-20 && stats.fun>=stats.maxFun-20 && stats.sleep >= stats.maxSleep-60 && stats.food>=stats.maxHappiness-20)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().color = Color.magenta;

        }
        else if(stats.food>stats.maxFood/3 && stats.fun > stats.maxFun/3 && stats.sleep > stats.maxSleep/3 && stats.happiness > stats.maxHappiness / 2)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().color = Color.yellow;
        }
        else 
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().color = Color.red;
        }
    }

    public void ToggleStats()
    {
        if (this.gameObject.activeInHierarchy)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }

}
