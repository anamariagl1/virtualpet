using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject pool;
    public float dificultad = 0.5f;
    public int contador = 1;
    void Start()
    {
        StartCoroutine(Minijuego());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator Minijuego()
    {
        while (true)
        {
           
            Coroutine generarMoneda = StartCoroutine(CrearMoneda());
            yield return new WaitForSeconds(20);
            StopCoroutine(generarMoneda);
            dificultad += 0.1f;
            contador++;            

        }
    }
    private IEnumerator CrearMoneda()
    {
        while (true)
        {
            int enemigo = Random.Range(0, 5);
            if (enemigo < 4)
            {
                for (int i = 0; i < pool.transform.childCount; i++)
                {
                    Transform nuevaMoneda = pool.transform.GetChild(i);

                    if (nuevaMoneda.gameObject.activeInHierarchy == false && nuevaMoneda.tag=="Coin")
                    {
                        nuevaMoneda.gameObject.SetActive(true);
                        nuevaMoneda.transform.position = new Vector2(Random.Range(-10f, 10), this.transform.position.y);
                        nuevaMoneda.GetComponent<Rigidbody2D>().gravityScale += dificultad;
                        break;
                    }
                }

            }
            else
            {
                for (int i = 0; i < pool.transform.childCount; i++)
                {
                    Transform nuevaMoneda = pool.transform.GetChild(i);

                    if (nuevaMoneda.gameObject.activeInHierarchy == false && nuevaMoneda.tag=="Bad")
                    {
                        nuevaMoneda.gameObject.SetActive(true);
                        nuevaMoneda.transform.position = new Vector2(Random.Range(-10f, 10), this.transform.position.y);
                        nuevaMoneda.GetComponent<Rigidbody2D>().gravityScale += dificultad;
                        break;
                    }
                }
            }
            yield return new WaitForSeconds(1);
        }
    }
}
