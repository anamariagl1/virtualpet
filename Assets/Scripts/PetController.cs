using System;
using System.Collections;
using System.Data;
using Unity.VisualScripting;
using UnityEngine;

public class PetController : MonoBehaviour
{
    [SerializeField] private GameObject m_food;
    [SerializeField] private GameObject m_bed;
    [SerializeField] private GameObject gameManager;
    [SerializeField] private Sprite sprite_puppy;
    [SerializeField] private Sprite sprite_kitty;
    [SerializeField] private SO_Pet m_stats;
    public SO_Pet Stats { get { return m_stats; } }
    [SerializeField] private SO_Pet m_puppy;
    [SerializeField] private SO_Pet m_kitty;
    [SerializeField] private SO_Partida m_partida;
    [SerializeField] private bool ocupado;
    [SerializeField] private bool idle;
    [SerializeField] private StatsController statsText;
    public delegate void ChangeStats();
    public event ChangeStats OnChangeStats;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        animator = this.gameObject.GetComponent<Animator>();
        if (m_partida.tAnterior != "")
        {
            CalcularTiempo();
        }

        switch (gameManager.GetComponent<GameManager>().partida.id_mascota)
        {
            case 0:
                m_stats = m_puppy;
                this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite_puppy;
                break;
            case 1:
                m_stats = m_kitty;
                this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite_kitty;
                break;

        }
        OnChangeStats += statsText.Actualizar;
        m_food.GetComponent<FoodController>().OnComer += this.Comer;
        StartCoroutine(Hambre());
        StartCoroutine(Sleep());
        StartCoroutine(Bored());
        StartCoroutine(Rutina());
        ocupado = false;
        StartCoroutine(Idle());
    }

    // Update is called once per frame
    void Update()
    {
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Food" && m_stats.food < m_stats.maxFood-20)
        {
            StartCoroutine(Comiendo());

        }
        else if (collision.gameObject.tag == "Bed" && m_stats.sleep < m_stats.maxSleep / 3)
        {
            StartCoroutine(Durmiendo());
        }
    }

    private IEnumerator Rutina()
    {
        while (true)
        {
            if (m_stats.food <= 40 && m_food.GetComponent<FoodController>().food.capacity > 0) //Comer
            {
                Comer();
            }
            else if (m_stats.sleep <= 20 && !ocupado) //Mimir
            {
                Mimir();
            }
            else if (!ocupado && !idle)
            {
                int random = UnityEngine.Random.Range(0, 5);
                if (random < 4) //Moverse
                {
                    Vector2 direccion = -new Vector2(UnityEngine.Random.Range(-300f, 300f), UnityEngine.Random.Range(-300f, 300f)).normalized;
                    Moverse(direccion);
                }
                else //Idle
                {
                    StartCoroutine(Idle());
                }
            }
            yield return new WaitForSeconds(1);
        }
    }
    private IEnumerator Hambre()
    {
        while (true)
        {
            if (m_stats.food > 0)
            {
                m_stats.food -= 10;
            }
            else if (m_stats.happiness > 0)
            {
                m_stats.happiness -= 5;
            }
            OnChangeStats?.Invoke();
            yield return new WaitForSeconds(3);
        }
    }
    private IEnumerator Sleep()
    {
        while (true)
        {
            if (m_stats.sleep > 0)
            {
                m_stats.sleep -= 10;
            }
            else if (m_stats.happiness > 0)
            {
                m_stats.happiness -= 5;
            }
            OnChangeStats?.Invoke();
            yield return new WaitForSeconds(10);
        }
    }

    private IEnumerator Bored()
    {
        while (true)
        {
            if (m_stats.fun > 0)
            {
                m_stats.fun -= 10;
            }
            else if (m_stats.happiness > 0)
            {
                m_stats.happiness -= 5;
            }
            OnChangeStats?.Invoke();
            yield return new WaitForSeconds(10);
        }
    }

    private IEnumerator Idle()
    {
        switch (m_partida.id_mascota)
        {
            case 0:
                animator.Play("puppy_idle");
                break;
            case 1:
                animator.Play("kitty_idle");
                break;
        }
        idle = true;
        this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        yield return new WaitForSeconds(2);
        idle = false;
    }

    private void CalcularTiempo()
    {
        DateTime tAnterior = Convert.ToDateTime(m_partida.tAnterior);
        DateTime tActual = DateTime.Now;
        double time = (tActual - tAnterior).TotalHours; //TotalMinutes o TotalHours
        print("Tiempo: " + time);
        Hambre(time);
    }

    private void Hambre(double hours)
    {
        if (hours > 48)
        {
            m_stats.food -= 100;
        }
        else if (hours > 24)
        {
            m_stats.food -= 80;
        }
        else if (hours > 12)
        {
            m_stats.food -= 60;
        }
        else if (hours > 6)
        {
            m_stats.food -= -40;
        }
        else if (hours > 3)
        {
            m_stats.food -= -20;
        }
        if (m_stats.food < 0)
        {
            m_stats.food = 0;
        }
        OnChangeStats?.Invoke();
    }

    private IEnumerator Comiendo()
    {
        while (m_stats.food < m_stats.maxFood && m_food.GetComponent<FoodController>().food.capacity > 0)
        {
            print("Comiendo");            
            this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            this.GetComponent<ParticleSystem>().Play();
            m_stats.food += 20;
            if (m_stats.food > m_stats.maxFood)
            {
                m_stats.food = m_stats.maxFood;
            }
            SerFeliz(5);
            m_food.GetComponent<FoodController>().LoseFood(20);
            OnChangeStats?.Invoke();
            yield return new WaitForSeconds(1);
        }
        ocupado = false;
    }

    private IEnumerator Durmiendo()
    {
        switch (m_partida.id_mascota)
        {
            case 0:
                animator.Play("puppy_sleep");
                break;
            case 1:
                animator.Play("kitty_sleep");
                break;
        }
        while (m_stats.sleep < m_stats.maxSleep)
        {
            ocupado = true;
            print("Mimiendo");
            this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            m_stats.sleep += 20;
            if (m_stats.sleep > m_stats.maxSleep)
            {
                m_stats.sleep = m_stats.maxSleep;
            }
            SerFeliz(5);
            OnChangeStats?.Invoke();
            yield return new WaitForSeconds(1);
        }
        ocupado = false;
    }

    private void Moverse(Transform transform)
    {
        if (m_stats.food > 30)
        {
            switch (m_partida.id_mascota)
            {
                case 0:
                    animator.Play("puppy_walk");
                    break;
                case 1:
                    animator.Play("kitty_walk");
                    break;
            }
        }
        else
        {
            switch (m_partida.id_mascota)
            {
                case 0:
                    animator.Play("puppy_sad_walk");
                    break;
                case 1:
                    animator.Play("kitty_sad_walk");
                    break;
            }
        }
        Vector2 direccion = transform.position - this.transform.position;
        direccion.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = direccion * 2;
        if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

    }

    private void Moverse(Vector2 direccion)
    {
        switch (m_partida.id_mascota)
        {
            case 0:
                animator.Play("puppy_walk");
                break;
            case 1:
                animator.Play("kitty_walk");
                break;
        }
        this.GetComponent<Rigidbody2D>().velocity = direccion * 2;
        if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

    }

    public void Comer()
    {
        ocupado = true;
        print("Voy a comer");
        Moverse(m_food.transform);

    }

    public void Mimir()
    {
        ocupado = true;
        print("Voy a mimir");
        Moverse(m_bed.transform);
    }

    public void SerFeliz(int felicidad)
    {
        if (m_stats.happiness < m_stats.maxHappiness)
        {
            m_stats.happiness += felicidad;
        }
        if (m_stats.happiness > m_stats.maxHappiness)
        {
            m_stats.happiness = m_stats.maxHappiness;
        }
    }

    public void Divertirse(int puntos)
    {
        m_stats.fun += puntos;
        if(m_stats.fun > m_stats.maxFun)
        {
            m_stats.fun = m_stats.maxFun;
        }
        SerFeliz(40);
    }

}
