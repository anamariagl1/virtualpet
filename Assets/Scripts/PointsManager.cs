using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsManager : MonoBehaviour
{
    [SerializeField] private SO_Partida money;
    [SerializeField] private SO_Partida points;
    void Start()
    {
        points.amount = 0;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = points.amount.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void sumarCantidad()
    {
        points.amount += 10;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = points.amount.ToString();
    }
    public void sumarCantidad(int cantidad)
    {
        points.amount += cantidad;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = points.amount.ToString();
    }
    public void sumarPuntosATotal()
    {
        money.amount += points.amount;
        points.amount = 0;
    }
}
