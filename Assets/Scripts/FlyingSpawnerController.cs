using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingSpawnerController : MonoBehaviour
{
    public GameObject pool;
    public float velocidad;
    void Start()
    {
        StartCoroutine(Minijuego());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator Minijuego()
    {
        while (true)
        {
            for (int i = 0; i < pool.transform.childCount; i++)
            {
                Transform nuevoObstaculo = pool.transform.GetChild(i);

                if (nuevoObstaculo.gameObject.activeInHierarchy == false)
                {
                    nuevoObstaculo.gameObject.SetActive(true);
                    nuevoObstaculo.transform.position = new Vector2(this.transform.position.x, Random.Range(-2.5f, 2.5f));
                    nuevoObstaculo.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocidad, 0);
                    break;
                }
            }

            yield return new WaitForSeconds(1.5f);          

        }
    }
}
