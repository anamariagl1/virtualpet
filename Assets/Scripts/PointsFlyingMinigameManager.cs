using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsFlyingMinigameManager : MonoBehaviour
{
    [SerializeField] PointsManager pointsManager;
    [SerializeField] int puntos;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SumarPuntos());
    }

    private IEnumerator SumarPuntos()
    {
        while (true)
        {
            pointsManager.sumarCantidad(puntos);
            yield return new WaitForSeconds(1);
        }
    }
}
