using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Pet : ScriptableObject
{
    public int id;
    public int maxFood;
    public int maxHappiness;
    public int maxSleep;
    public int maxFun;
    public int food;
    public int sleep;
    public int fun;
    public int happiness;
    public Sprite sprite;

}
