using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Vector2 = UnityEngine.Vector2;

public class PetMinigameController : MonoBehaviour
{
    public int hp;

    public delegate void Points();
    public event Points OnPoints; 
    public delegate void Morirse();
    public event Morirse OnMorirse;
    [SerializeField] private InputActionAsset InputAsset;
    private InputActionAsset Input;
    private InputAction PointerPosition;
    [SerializeField] private PointsManager pointsManager;
    [SerializeField] private GameObject gameManager;
    [SerializeField] private SO_Partida partida;
    [SerializeField] private ButtonController button;
    [SerializeField] private Sprite puppy;
    [SerializeField] private Sprite kitty;
    [SerializeField] private int velocity;
    [SerializeField] private AudioClip puppy_audio;
    [SerializeField] private AudioClip kitty_audio;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager");
        Input = Instantiate(InputAsset);
        Input.FindActionMap("CoinMinigame").FindAction("Movement").performed += Moverse;
        PointerPosition = Input.FindActionMap("CoinMinigame").FindAction("Position");
        Input.FindActionMap("CoinMinigame").FindAction("Movement").canceled += Stop;
        Input.FindActionMap("CoinMinigame").Enable();

        OnPoints += pointsManager.sumarCantidad;
        OnMorirse += pointsManager.sumarPuntosATotal;

        switch (gameManager.GetComponent<GameManager>().partida.id_mascota)
        {
            case 0:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = puppy;
                break;
            case 1:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = kitty;
                break;

        }
    }
    void Start()
    {
        hp = 3;
        if (partida.id_mascota == 0)
        {
            this.gameObject.GetComponent<AudioSource>().clip = puppy_audio;
        }
        else
        {
            this.gameObject.GetComponent<AudioSource>().clip = kitty_audio;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin")
        {
            OnPoints?.Invoke();
            this.gameObject.GetComponent<ParticleSystem>().Play();
        }
        else if(collision.tag == "Bad")
        {
            hp--;
            this.gameObject.GetComponent<AudioSource>().Play();
            StartCoroutine(Herido());
            if (hp <= 0)
            {
                OnMorirse?.Invoke();
                Input.FindActionMap("CoinMinigame").Disable();
                button.EndMinigame();
            }
        }
    }

    private IEnumerator Herido()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
    private void Moverse(InputAction.CallbackContext context)    
    {        
        Vector2 vector = PointerPosition.ReadValue<Vector2>();
        if (vector.x > Screen.width/2)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0);
        }
        else
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0);
        }
    }
    private void Stop(InputAction.CallbackContext context)
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);   
    }
}
