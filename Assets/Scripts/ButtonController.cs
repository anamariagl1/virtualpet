using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    [SerializeField] private SO_Partida partida;

    public void Start()
    {
        
    }

    public void ContinueGame()
    {
        if (partida.tAnterior != "" && partida.tAnterior != null){
            House();
        }
    }
    public void House()
    {
        SceneManager.LoadScene("HouseScene");
    }

    public void SelectMinigame()
    {
        SceneManager.LoadScene("SelectMinigameScene");
    }
    public void Minigame1()
    {
        SceneManager.LoadScene("CoinMinigameScene");
    }

    public void Minigame2()
    {
        SceneManager.LoadScene("FlyingMinigameScene");
    }

    public void EndMinigame()
    {

        House();
    }

    public void Menu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void NuevaPartida()
    {
        SceneManager.LoadScene("NuevaPartidaScene");
    }
    public void Retry()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
